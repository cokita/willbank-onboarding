import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  title: string = '';
  message: string = '';
  isConfirm: boolean = true;
  iconTitle: string = '';
  iconDesc: string = '';
  textButtonOk: string = 'Fechar';
  constructor(public dialogRef: MatDialogRef<ConfirmComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.isConfirm = (this.data.isConfirm === undefined);
    this.iconTitle = (this.data.iconTitle === undefined) ? null : this.data.iconTitle;
    this.iconDesc = (this.data.iconDesc === undefined) ? null : this.data.iconDesc;
    this.textButtonOk = (this.data.textButtonOk === undefined) ? null : this.data.textButtonOk;
  }

}
