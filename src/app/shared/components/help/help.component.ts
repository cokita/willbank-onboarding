import { MediaQueryListCustom } from './../../models/media-query-list-custom.model';
import { GeneralService } from './../../services/general.service';
import { ChangeDetectorRef, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmComponent } from '../confirm/confirm.component';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HelpComponent implements OnInit {
  private _title: string = '';
  @Input() set title(value: string) {
    this._title = value;
  }

  private _text: string = '';
  @Input() set text(value: string) {
    this._text = value;
  }
  constructor(private dialog: MatDialog, private generalService: GeneralService) { }

  ngOnInit(): void {
  }

  get title() {return this._title};
  get text() {return this._text};
  open() {
    const dialogConfig = new MatDialogConfig();
    this.generalService.screenMatch.subscribe((response: MediaQueryListCustom)  => {
      if(!response.isMobile && !response.isTablet) {
          dialogConfig.maxWidth = '40vw';
        }
      })
      dialogConfig.data = {
        title: this.title,
        message: this.text,
        textButtonOk: 'Ok, entendi',
        isConfirm: false
      }
      let dialogOpened = this.dialog.open(ConfirmComponent, dialogConfig);
  }

}
