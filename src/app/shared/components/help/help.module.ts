import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelpComponent } from './help.component';
import { MatIconModule } from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import { ConfirmModule } from '../confirm/confirm.module';
import { FlexLayoutModule } from '@angular/flex-layout';



@NgModule({
  declarations: [
    HelpComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatDialogModule,
    FlexLayoutModule
  ],
  exports: [HelpComponent]
})
export class HelpModule { }
