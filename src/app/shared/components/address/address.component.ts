import { ViaCepResult } from './../../models/address.model';
import { AddressService } from './../../services/address.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { States } from '../../models/state.model';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {
  addressForm!: FormGroup;
  @Output() formChangeEvent = new EventEmitter();

  private _submitted: boolean = false;
  @Input() set submitted(value: boolean) {
    this._submitted = value;
    if(value && this.addressForm){
      this.addressForm.markAllAsTouched();
    }
  }
  formProfile!: FormGroup;
  states = States;
  constructor(private formBuilder: FormBuilder, private addressService: AddressService) { }

  ngOnInit(): void {
    this.formInit();
    this.valuesChanges();
     //retirar
    //  this.addressForm.controls.zipcode.setValue(58037030)
    //  this.getAddresByZipcode();
    //  this.f.number.setValue(505);
    //  this.f.complement.setValue('apto 201 A');
    //  this.f.landmark.setValue('Em frente ao porto pinheiro');
  }

  formInit() {
      this.addressForm = this.formBuilder.group({
        zipcode: [null, [Validators.required]],
        street: [null, [Validators.required]],
        number: [null, Validators.required],
        complement: [null],
        neighborhood: [null, Validators.required],
        state: [null, Validators.required],
        city: [null, Validators.required],
        cityAndState: [null],
        landmark: [null]
      })

  }

  get f() {return this.addressForm.controls;}
  get submitetd() {return this._submitted};


  getAddresByZipcode() {
    this.addressService.getAddressByZipcode(this.f.zipcode.value).subscribe((result: ViaCepResult) => {
      this.addressForm.patchValue({
        street: result.logradouro,
        neighborhood: result.bairro,
        city: result.localidade,
        state: result.uf,
        cityAndState: `${result.localidade}/${result.uf}`
      })
    })
  }
  valuesChanges() {
    this.addressForm.valueChanges.subscribe(res => {
      this.formChangeEvent.emit(this.addressForm.value);
    })
  }

}
