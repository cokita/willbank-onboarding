import { MaterialFormSharedModule } from './../../material/material-form-shared.module';
import { AddressComponent } from './address.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxMaskModule } from 'ngx-mask';



@NgModule({
  declarations: [AddressComponent],
  imports: [
    CommonModule,
    MaterialFormSharedModule,
    FlexLayoutModule,
    NgxMaskModule
  ],
  exports: [AddressComponent]
})
export class AddressModule { }
