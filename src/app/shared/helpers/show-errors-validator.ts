import { FormGroup, ValidationErrors } from "@angular/forms";
/**
 * Apenas para mostrar no console o erro que existe no formulário, as vezes não conseguimos entender por que o form não está válido.
 * Esse método printa no console o motivo.
 * @param form
 */
export function showErrorsValidatorForm(form: FormGroup) {

  console.log('%c ==>> Validation Errors: ', 'color: red; font-weight: bold; font-size:25px;');

  let totalErrors = 0;

  Object.keys(form.controls).forEach(key => {
    const controlErrors: ValidationErrors | null | undefined = form.get(key)?.errors;
    if (controlErrors != null) {
       totalErrors++;
       Object.keys(controlErrors).forEach(keyError => {
         console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        });
    }
  });

  console.log('Number of errors: ' ,totalErrors);
}
