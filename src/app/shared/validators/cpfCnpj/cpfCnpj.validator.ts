import {AbstractControl, ValidationErrors} from '@angular/forms';
import { CpfCnpj } from './cpf-cnpj';

export function cpfCnpjValidator(control: AbstractControl): ValidationErrors | null {
  let cpfCnpj = typeof control.value === 'string' && control.value.replace(/([^\da-zA-Z])/g, '') || String(control.value);
  let cpfCnpjValidator = new CpfCnpj();
  let result = null;
  if(cpfCnpj.length <= 11) {
    result = cpfCnpjValidator.validarCPF(cpfCnpj);
  } else if(cpfCnpj.length > 11 && cpfCnpj.length <= 14){
    result = cpfCnpjValidator.validarCNPJ(cpfCnpj);
  }
  return result;
}


