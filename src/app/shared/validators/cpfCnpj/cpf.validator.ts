import {AbstractControl, ValidationErrors} from '@angular/forms';
import { CpfCnpj } from './cpf-cnpj';

export function cpfValidator(control: AbstractControl): ValidationErrors | null {
  let cpf = typeof control.value === 'string' && control.value.replace(/([^\da-zA-Z])/g, '') || String(control.value);
  let cpfCnpjValidator = new CpfCnpj();
  return cpfCnpjValidator.validarCPF(cpf);
}

