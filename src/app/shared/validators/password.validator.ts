import {AbstractControl, ValidationErrors} from '@angular/forms';

export function passwordValidator(control: AbstractControl): ValidationErrors | null {
  let error: any = {};
  let password = control.value;

  if(password && password.length < 8) {
    error['password'] = { min: true, message: "A senha deve ter no mínimo 8 caracteres." }
    control.setErrors(error);
  } else if(!/[0-9]{2}/gm.test(password)){
    error['password'] = { numeros: true, message: "A senha deve ter pelo menos 2 números." }
    control.setErrors(error);
  } else {
    error = null;
  }
  return error
}
