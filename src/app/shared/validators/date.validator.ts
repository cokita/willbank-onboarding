import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';
import * as moment from 'moment';

export function dateValidator(format?: string): ValidatorFn {
  return (c: AbstractControl): ValidationErrors | null=> {
    if(!format) format = "MM-DD-YYYY HH:mm:ss"
    let date = c.value;
    let newDate: moment.Moment = moment(date, format);
    if (newDate.isValid())
        return null;

    return { 'invalidDate': {valid: false }};
  }
}
