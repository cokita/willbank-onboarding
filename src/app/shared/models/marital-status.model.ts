export interface MaritalStatus {
  id: number;
  name: string;
}


export const MaritalStatus: MaritalStatus[] = [
  { id: 1, name: 'Solteiro' },
  { id: 2, name: 'Casado' },
  { id: 3, name: 'Separado' },
  { id: 4, name: 'Divorciado' },
  { id: 5, name: 'Viúvo' },
]
