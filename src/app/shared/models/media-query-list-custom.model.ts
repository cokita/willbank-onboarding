export interface MediaQueryListCustom extends MediaQueryList{
  isMobile: boolean,
  isTablet: boolean,
  isLarge: boolean,
  isExtraLarge: boolean
}
