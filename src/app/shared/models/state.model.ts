export interface State {
  abbreviation: string;
  state: string;
}


export const States: any[] = [
  { abbreviation: 'AC', name: 'Acre' },
  { abbreviation: 'AL', name: 'Alagoas' },
  { abbreviation: 'AP', name: 'Amapá' },
  { abbreviation: 'AM', name: 'Amazonas' },
  { abbreviation: 'BA', name: 'Bahia' },
  { abbreviation: 'CE', name: 'Ceará' },
  { abbreviation: 'DF', name: 'Distrito Federal' },
  { abbreviation: 'ES', name: 'Espírito Santo' },
  { abbreviation: 'GO', name: 'Goiás' },
  { abbreviation: 'MA', name: 'Maranhão' },
  { abbreviation: 'MT', name: 'Mato Grosso' },
  { abbreviation: 'MS', name: 'Mato Grosso do Sul' },
  { abbreviation: 'MG', name: 'Minas Gerais' },
  { abbreviation: 'PA', name: 'Pará' },
  { abbreviation: 'PB', name: 'Paraíba' },
  { abbreviation: 'PR', name: 'Paraná' },
  { abbreviation: 'PE', name: 'Pernambuco' },
  { abbreviation: 'PI', name: 'Piauí' },
  { abbreviation: 'RJ', name: 'Rio de Janeiro' },
  { abbreviation: 'RN', name: 'Rio Grande do Norte' },
  { abbreviation: 'RS', name: 'Rio Grande do Sul' },
  { abbreviation: 'RO', name: 'Rondônia' },
  { abbreviation: 'RR', name: 'Roraima' },
  { abbreviation: 'SC', name: 'Santa Catarina' },
  { abbreviation: 'SP', name: 'São Paulo' },
  { abbreviation: 'SE', name: 'Sergipe' },
  { abbreviation: 'TO', name: 'Tocantins' }
]

export enum states {
  AC ="Acre",
  AL = "Alagoas",
  AP = "Amapá",
  AM = "Amazonas",
  BA = "Bahia",
  CE = "Ceará",
  DF = "Distrito Federal",
  ES = "Espirito Santo",
  GO = "Goiás",
  MA = "Maranhão",
  MT = "Mato Grosso",
  MS = "Mato Grosso do Sul",
  MG = "Minas Gerais",
  PA = "Pará",
  PB = "Paraiba",
  PR = "Paraná",
  PE = "Pernambuco",
  PI = "Piauí",
  RJ = "Rio de Janeiro",
  RN = "Rio Grande do Norte",
  RS = "Rio Grande do Sul",
  RO = "Rondônia",
  RR = "Roraima",
  SC = "Santa Catarina",
  SP = "São Paulo",
  SE = "Sergipe",
  TO = "Tocantis"
}
