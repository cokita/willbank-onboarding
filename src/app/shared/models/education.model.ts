export interface Education {
  id: number;
  name: string;
}


export const Education: Education[] = [
  { id: 1, name: 'Educação infantil' },
  { id: 2, name: 'Fundamental' },
  { id: 3, name: 'Médio' },
  { id: 4, name: 'Superior (Graduação)' },
  { id: 5, name: 'Pós-graduação' },
  { id: 6, name: 'Mestrado' },
  { id: 7, name: 'Doutorado' },
]
