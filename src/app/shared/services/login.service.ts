import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from './http.service';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpService: HttpService, private router: Router) { }

  public getCurrentUser() {
    const currentUser = localStorage.getItem('currentUser');
    return currentUser ? JSON.parse(currentUser) : null;
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.clear();
    this.router.navigate(['/']);
  }

}
