import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private httpClient: HttpClient) { }

  getAddressByZipcode(zipcode? :string): Observable<any>{
    return this.httpClient.get(`https://viacep.com.br/ws/${zipcode}/json/`);
  }

}
