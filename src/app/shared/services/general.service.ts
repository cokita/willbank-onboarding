import { MediaQueryListCustom } from './../models/media-query-list-custom.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { MediaMatcher } from '@angular/cdk/layout';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  mdq!: MediaQueryList;
  mediaQueryListener!:()=>void;
  private screen = new BehaviorSubject<any>(false);
  constructor(private media: MediaMatcher) { }

  changeScreen(typeScreen: any) {
    this.screen.next(typeScreen);
  }

  get screenMatch(): Observable<MediaQueryListCustom> {
    return this.screen.asObservable();
  }

  setScreen() {
    let screenObject: MediaQueryListCustom = {} as MediaQueryListCustom;
    const mobileQuery = this.media.matchMedia('(max-width: 599px)');
    const tabletQuery = this.media.matchMedia('(min-width: 600px) and (max-width: 1000px)');
    const largeQuery = this.media.matchMedia('(min-width: 1001px) and (max-width: 1300px)');
    const extraLargeQuery = this.media.matchMedia('(min-width: 1300px)');
  if(tabletQuery.matches){
    this.mdq = tabletQuery;
    this.changeScreen(Object.assign(tabletQuery, screenObject, {isTablet: true}));
  } else if(mobileQuery.matches) {
    this.mdq = mobileQuery;
    this.changeScreen(Object.assign(mobileQuery, screenObject, {isMobile: true}));
  } else if(largeQuery.matches) {
    this.mdq = largeQuery;
    this.changeScreen(Object.assign(largeQuery, screenObject, {isLarge: true}));
  } else {
    this.mdq = extraLargeQuery;
    this.changeScreen(Object.assign(extraLargeQuery, screenObject, {isExtraLarge: true}));
  }
    this.mediaQueryListener = () => {
      if(tabletQuery.matches){
        this.changeScreen(Object.assign(tabletQuery, screenObject, {isTablet: true}));
      } else if(mobileQuery.matches) {
        this.changeScreen(Object.assign(mobileQuery, screenObject, {isMobile: true}));
      } else if(largeQuery.matches) {
        this.changeScreen(Object.assign(largeQuery, screenObject, {isLarge: true}));
      } else {
        this.changeScreen(Object.assign(extraLargeQuery, screenObject, {isExtraLarge: true}));
      }
    }
    if(this.mdq && this.mdq.addEventListener){
      this.mdq.addEventListener('change', this.mediaQueryListener);
    }
  }
}
