import { OnboardingFinishedComponent } from './pages/onboarding/onboarding-finished/onboarding-finished.component';
import { OnboardingComponent } from './pages/onboarding/onboarding.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: OnboardingComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/onboarding/onboarding.module').then(m => m.OnboardingModule),
      }
    ]
  },
  {
    path: 'finished',
    component: OnboardingFinishedComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExternalRoutingModule { }
