import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-onboarding-finished',
  templateUrl: './onboarding-finished.component.html',
  styleUrls: ['./onboarding-finished.component.scss']
})
export class OnboardingFinishedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
