import { AddressModule } from './../../../shared/components/address/address.module';
import { HelpModule } from './../../../shared/components/help/help.module';
import { MaterialFormSharedModule } from './../../../shared/material/material-form-shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OnboardingRoutingModule } from './onboarding-routing.module';
import { OnboardingComponent } from './onboarding.component';
import { Step1Component } from './steps/step1/step1.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StepsComponent } from './steps/steps.component';
import {MatStepperModule} from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { NgxMaskModule } from 'ngx-mask';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { Step2Component } from './steps/step2/step2.component';
import { CodeInputModule } from 'angular-code-input';
import { Step3Component } from './steps/step3/step3.component';
import { Step4Component } from './steps/step4/step4.component';
import { Step5Component } from './steps/step5/step5.component';
import { Step6Component } from './steps/step6/step6.component';
import { Step7Component } from './steps/step7/step7.component';
import { Step8Component } from './steps/step8/step8.component';
import { Step9Component } from './steps/step9/step9.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { OnboardingFinishedComponent } from './onboarding-finished/onboarding-finished.component';

@NgModule({
  declarations: [
    OnboardingComponent,
    Step1Component,
    StepsComponent,
    Step2Component,
    Step3Component,
    Step4Component,
    Step5Component,
    Step6Component,
    Step7Component,
    Step8Component,
    Step9Component,
    OnboardingFinishedComponent
  ],
  imports: [
    CommonModule,
    OnboardingRoutingModule,
    FlexLayoutModule,
    MatStepperModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatProgressBarModule,
    MaterialFormSharedModule,
    NgxMaskModule,
    MatSlideToggleModule,
    HelpModule,
    CodeInputModule,
    AddressModule,
    MatButtonToggleModule
  ]
})
export class OnboardingModule { }
