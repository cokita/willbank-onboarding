import { Router } from '@angular/router';
import { AfterContentChecked, ChangeDetectorRef, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StepsComponent implements OnInit, AfterContentChecked {
  progress: number = 0;
  stepperIndex: number = 0;
  stepsCount: number = 3;
  allData: any = {};
  @ViewChild('stepper') private stepper!: MatStepper;
  addressForm!: FormGroup;
  constructor(private formBuilder: FormBuilder, private cdr: ChangeDetectorRef, private router: Router) { }
  ngAfterContentChecked(): void {
    if(this.stepper){
      this.progress = this.stepper ? ((this.stepper.selectedIndex+1) *100)/this.stepper.steps.length : 0.
      this.stepperIndex = this.stepper ? (this.stepper.selectedIndex+1) : 1;
      this.stepsCount = this.stepper.steps.length;

    }
  }

  ngOnInit(): void {

  }

  formInit() {
    this.addressForm = this.formBuilder.group({
      id: [null],
      zipCode: [null, Validators.required],
      state: [null, Validators.required],
      city: [null, Validators.required],
      neighborhood: [null, Validators.required],
      address: [null, Validators.required],
      complement: [],
      number: [null, Validators.required],
    });
  }

  formSended(data: any) {
    this.stepper.next();
    console.log(data)
    this.allData = Object.assign({...this.allData}, data);
    if(this.allData?.step == 9) {
      this.router.navigate(['/finished']);
    }
    console.log(data, this.allData)
  }

}
