import { MaritalStatus } from './../../../../../shared/models/marital-status.model';
import { Education } from './../../../../../shared/models/education.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { showErrorsValidatorForm } from 'src/app/shared/helpers/show-errors-validator';

@Component({
  selector: 'app-step5',
  templateUrl: './step5.component.html',
  styleUrls: ['./step5.component.scss']
})
export class Step5Component implements OnInit {
  @Output() formSubmit = new EventEmitter();
  formProfile!: FormGroup;
  submitted: boolean = false;
  education = Education;
  maritalStatus = MaritalStatus;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formInit();
  }

  get f() {return this.formProfile.controls}

  formInit() {
    this.formProfile = this.formBuilder.group({
      mothersName: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(40)]],
      education: [null, [Validators.required]],
      occupation: [null, Validators.required],
      monthlyIncome: [null, Validators.required],
      patrimony: [null, Validators.required],
      maritalStatus: [null, Validators.required],
      spouse: [null, [Validators.minLength(3), Validators.maxLength(40)]]
    })

  }

  save() {
    this.submitted = true;
    if(this.formProfile.valid){
      this.formSubmit.emit({step: 5, profile: this.formProfile.value})
    }
  }

}
