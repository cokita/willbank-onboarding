import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-step8',
  templateUrl: './step8.component.html',
  styleUrls: ['./step8.component.scss']
})
export class Step8Component implements OnInit {
  @Output() formSubmit = new EventEmitter();
  formPep!: FormGroup;
  submitted: boolean = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formInit();
  }

  get f() {return this.formPep.controls}

  formInit() {
    this.formPep = this.formBuilder.group({
      pep: [false],
    })

  }

  save() {
    this.submitted = true;
    if(this.formPep.valid){
      this.formSubmit.emit({step: 8, pep: this.formPep.value})
    }
  }
}
