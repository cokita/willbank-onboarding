import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-step7',
  templateUrl: './step7.component.html',
  styleUrls: ['./step7.component.scss']
})
export class Step7Component implements OnInit {
  @Output() formSubmit = new EventEmitter();
  private _alldata: any;
  @Input() set alldata(value: any) {
    this._alldata = value;
  }
  addressFormBase!: FormGroup;
  submitted: boolean = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formInit();
  }

  formInit() {
    this.addressFormBase = this.formBuilder.group({
      newAddress: [false],
      zipcode: [null],
      street: [null],
      number: [null],
      complement: [null],
      neighborhood: [null],
      cityAndState: [{value: null, disabled: true}],
      landmark: [null]
    })

  }

  valueChanges() {
    this.f.newAddress.valueChanges.subscribe((val:boolean) => {
      if(!val) {
        this.addressFormBase.clearValidators();
      } else{
        this.f.zipcode.addValidators(Validators.required);
        this.f.street.addValidators(Validators.required);
        this.f.number.addValidators(Validators.required);
        this.f.neighborhood.addValidators(Validators.required);
        this.f.cityAndState.addValidators(Validators.required);
      }
    })
  }

  get f() {return this.addressFormBase.controls};
  get alldata() {return this._alldata};

  addressFormChange(value: any) {
    this.addressFormBase.patchValue(value);
  }

  save() {
    this.addressFormBase.markAsTouched();
    this.submitted = true;
    if(this.addressFormBase.valid){
      this.formSubmit.emit({step: 7, deliveryAddress: this.addressFormBase.value})
    }
  }

}
