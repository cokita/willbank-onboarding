import { showErrorsValidatorForm } from 'src/app/shared/helpers/show-errors-validator';
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { cpfValidator } from 'src/app/shared/validators/cpfCnpj/cpf.validator';
import { passwordValidator } from 'src/app/shared/validators/password.validator';
import { dateValidator } from 'src/app/shared/validators/date.validator';

export const userTeste = {
  document: '00331948150',
  name: 'Ana Flávia Carvalho',
  birthday: '31/07-1985',
  cellphone: '61996180178',
  email: 'anaflavia.alpc@gmail.com',
  password: 'Abcd1234##',
  acceptUserContract: true,
  acceptTerms: true
}

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Step1Component implements OnInit {
  @Output() formSubmit = new EventEmitter();
  hidePassword: boolean = true;
  submitted = false;
  validatorsNames: ValidatorFn[] = [Validators.required, Validators.minLength(3), Validators.maxLength(40)];
  formOnboarding!: FormGroup;
  constructor(private formBuilder: FormBuilder, private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.formInit();
  }

  get f() {return this.formOnboarding.controls}

  formInit() {
    this.formOnboarding = this.formBuilder.group({
      document: [null, [Validators.required, cpfValidator]],
      name: [null, this.validatorsNames],
      hasSocialName: [false, Validators.required],
      socialName: [null],
      civilName: [null],
      birthday: [null, [Validators.required, dateValidator('DD/MM/YYYY')]],
      cellphone: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, passwordValidator]],
      acceptUserContract: [false, Validators.requiredTrue],
      acceptTerms: [false, Validators.requiredTrue]
    })
    // this.formOnboarding.patchValue(userTeste)
    this.f.hasSocialName.valueChanges.subscribe((val:boolean) => {
      if(val) {
        this.f.name.disable();
        this.f.name.clearValidators();
        this.f.socialName.addValidators(this.validatorsNames);
        this.f.civilName.addValidators(this.validatorsNames);
        this.f.civilName.updateValueAndValidity({onlySelf: true});
        this.f.socialName.updateValueAndValidity({onlySelf: true})
      } else{
        this.f.name.enable();
        this.f.name.addValidators(this.validatorsNames);
        this.formOnboarding.controls.socialName.clearValidators();
        this.formOnboarding.controls.civilName.clearValidators();
        this.formOnboarding.controls.socialName.updateValueAndValidity({onlySelf: true});
        this.formOnboarding.controls.civilName.updateValueAndValidity({onlySelf: true});
      }
    })
  }

  save() {
    this.submitted = true;
    if(this.formOnboarding.valid){
      this.formSubmit.emit({step: 1, user: this.formOnboarding.value})
    }
  }
}
