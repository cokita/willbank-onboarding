import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class Step2Component implements OnInit {
  @Output() formSubmit = new EventEmitter();
  private _alldata: any;
  @Input() set alldata(value: any) {
    this._alldata = value;
    if(this.formEmailCode){
      this.formEmailCode.patchValue({email: this.allData?.user?.email})
    }
  }
  formEmailCode!: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formInit();
  }

  get f() { return this.formEmailCode.controls};
  get allData() {return this._alldata};

  formInit() {
    this.formEmailCode = this.formBuilder.group({
      code: [null, [Validators.required, Validators.minLength(6)]],
      email: [{value: null, disabled:true}, [Validators.required, Validators.email]]
    });
  }

  save() {
    this.submitted = true;
    if(this.formEmailCode.valid){
      this.formSubmit.emit({step: 2, emailCode: this.formEmailCode.value})
    }
  }

  onCodeChanged(event: any){
    this.f.code.setValue(event)
  }

  onCodeCompleted(event: any){
    this.f.code.setValue(event);
  }

}
