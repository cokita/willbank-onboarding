import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss']
})
export class Step3Component implements OnInit {
  private _alldata: any;
  @Output() formSubmit = new EventEmitter();
  @Input() set alldata(value: any) {
    this._alldata = value;
    if(this.formCellphoneCode){
      this.formCellphoneCode.patchValue({cellphone: this.alldata?.user?.cellphone})
    }
  }
  formCellphoneCode!: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formInit();
  }

  get f() { return this.formCellphoneCode.controls};
  get alldata() { return this._alldata};

  formInit() {
    this.formCellphoneCode = this.formBuilder.group({
      code: [null, [Validators.required, Validators.minLength(6)]],
      cellphone: [{value: null, disabled:true}, [Validators.required]]
    })
  }

  save() {
    this.submitted = true;
    if(this.formCellphoneCode.valid){
      this.formSubmit.emit({step: 3, cellphoneCode: this.formCellphoneCode.value})
    }
  }

  onCodeChanged(event: any){
    this.f.code.setValue(event)
  }

  onCodeCompleted(event: any){
    this.f.code.setValue(event);
  }

}
