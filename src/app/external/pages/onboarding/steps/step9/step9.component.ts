import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-step9',
  templateUrl: './step9.component.html',
  styleUrls: ['./step9.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Step9Component implements OnInit {
  @Output() formSubmit = new EventEmitter();
  formDuoDate!: FormGroup;
  possiblesDuoDate: number[] = [5,10,15,20,25]
  submitted: boolean = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formInit();
  }

  get f() {return this.formDuoDate.controls}

  formInit() {
    this.formDuoDate = this.formBuilder.group({
      duoDate: [null, Validators.required],
    })

  }

  save() {
    this.submitted = true;
    if(this.formDuoDate.valid){
      this.formSubmit.emit({step: 9, duoDate: this.formDuoDate.value})
    }
  }

  formatDate(dateNumber: number) {
    return String(dateNumber).padStart(2, '0');
  }
}
