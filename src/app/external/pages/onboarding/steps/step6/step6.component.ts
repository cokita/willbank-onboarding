import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-step6',
  templateUrl: './step6.component.html',
  styleUrls: ['./step6.component.scss']
})
export class Step6Component implements OnInit {
  @Output() formSubmit = new EventEmitter();
  addressFormBase!: FormGroup;
  submitted: boolean = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formInit();
  }

  formInit() {
    this.addressFormBase = this.formBuilder.group({
      zipcode: [null, [Validators.required]],
      street: [null, [Validators.required]],
      number: [null, Validators.required],
      complement: [null],
      neighborhood: [null, Validators.required],
      city: [null],
      state: [null],
      cityAndState: [{value: null, disabled: true}, Validators.required],
      landmark: [null]
    })

  }

  addressFormChange(value: any) {
    this.addressFormBase.patchValue(value);
  }

  save() {
    this.addressFormBase.markAsTouched();
    this.submitted = true;
    if(this.addressFormBase.valid){
      this.formSubmit.emit({step: 6, address: this.addressFormBase.value})
    }
  }

}
