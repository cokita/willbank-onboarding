import { States } from '../../../../../shared/models/state.model';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { dateValidator } from 'src/app/shared/validators/date.validator';
import * as moment from 'moment';

@Component({
  selector: 'app-step4',
  templateUrl: './step4.component.html',
  styleUrls: ['./step4.component.scss']
})
export class Step4Component implements OnInit {
  @Output() formSubmit = new EventEmitter();
  formDocument!: FormGroup;
  submitted: boolean = false;
  states = States;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formInit();
  }

  get f() {return this.formDocument.controls}

  formInit() {
    this.formDocument = this.formBuilder.group({
      documentType: [1],
      number: [null, [Validators.required]],
      state: [null, Validators.required],
      issuer: [null, Validators.required],
      expirationDate: [moment().format("DD/MM/YYYY"), [Validators.required, dateValidator('DD/MM/YYYY')]]
    })

  }

  save() {
    this.submitted = true;
    if(this.formDocument.valid){
      this.formSubmit.emit({step: 4, document: this.formDocument.value})
    }
  }

}
