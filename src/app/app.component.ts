import { GeneralService } from './shared/services/general.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'will-webapp';
  constructor(private generalService: GeneralService){
    this.generalService.setScreen();
  }
}
